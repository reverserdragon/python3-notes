# Python3 Notes

## Built-in Functions
### sum(iterable,start)
Returns the summed value of the iterable. E.g. you can use a tuple to return its sum. Optional start parameter is added to the final sum.
```
>>>sum(1,2,3,4)
10
>>>fin = (1,2,3)
>>>sum(fin)
6
>>>sum(fin,5)
11
```

### enumerate(iterable, start=0)
The enumerate() function adds a counter to an iterable.

So for each element in cursor, a tuple is produced with (counter, element); the for loop binds that to row_number and row, respectively.
```
>>> elements = ('foo', 'bar', 'baz')
>>> for elem in elements:
...     print elem
... 
foo
bar
baz
>>> for count, elem in enumerate(elements):
...     print count, elem
... 
0 foo
1 bar
2 baz
```